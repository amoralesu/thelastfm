package com.example.amoralesu.myapplication.io.model;

import com.example.amoralesu.myapplication.domain.Artist;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by amoralesu on 3/5/2016.
 */
public class HypedArtistsResponse {

    @SerializedName(JsonKeys.ARTISTS_RESULTS)
    private HypedArtistsResults results;

    public  ArrayList<Artist> getArtist(){
       return results.artists;
    }

    private class HypedArtistsResults {
        @SerializedName(JsonKeys.ARTIST_ARRAY)
        ArrayList<Artist> artists;
    }
}
