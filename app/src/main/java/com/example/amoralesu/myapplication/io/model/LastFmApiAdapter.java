package com.example.amoralesu.myapplication.io.model;

import com.example.amoralesu.myapplication.io.ApiConstans;
import com.example.amoralesu.myapplication.io.LastFmApiService;

import retrofit.RestAdapter;

/**
 * Created by amoralesu on 3/5/2016.
 */
public class LastFmApiAdapter {
    private static LastFmApiService API_SERVICE;

    public static LastFmApiService getApiService() {
        if (API_SERVICE == null) {
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstans.URL_BASE)
                    .setLogLevel(RestAdapter.LogLevel.BASIC)
                    .build();

            API_SERVICE = adapter.create(LastFmApiService.class);
        }
        return  API_SERVICE;
    }
}
