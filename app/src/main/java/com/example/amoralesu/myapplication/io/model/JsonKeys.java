package com.example.amoralesu.myapplication.io.model;

/**
 * Created by amoralesu on 3/5/2016.
 */
public class JsonKeys {
    public static final String ARTISTS_RESULTS = "artists";
    public static final String ARTIST_ARRAY = "artist";
}
