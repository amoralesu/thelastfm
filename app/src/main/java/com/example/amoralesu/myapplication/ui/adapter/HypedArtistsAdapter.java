package com.example.amoralesu.myapplication.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amoralesu.myapplication.R;
import com.example.amoralesu.myapplication.domain.Artist;

import java.util.ArrayList;


/**
 * Created by amoralesu on 3/5/2016.
 */
public class HypedArtistsAdapter extends RecyclerView.Adapter<HypedArtistsAdapter.HypedArtistViewHolder> {

    Context context;
    ArrayList<Artist> artists;

    public HypedArtistsAdapter(Context context) {
        this.context = context;
        this.artists = new ArrayList<>();
    }

    public class HypedArtistViewHolder extends RecyclerView.ViewHolder {
        TextView artistName;
        ImageView artistsImage;

        public HypedArtistViewHolder(View itemView) {
            super(itemView);
            artistName = (TextView) itemView.findViewById(R.id.txt_name);
            artistsImage = (ImageView) itemView.findViewById(R.id.img_artist);
        }

        public void setArtistName(String name) {
            artistName.setText(name);
        }

    }

    //Cuando se crea el view holder
    @Override
    public HypedArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_hyped_artists, parent, false);
        return new HypedArtistViewHolder(itemView);
    }

    //Conctar el view holder con los datos
    @Override
    public void onBindViewHolder(HypedArtistViewHolder holder, int position) {
        //super.onBindViewHolder(holder, position);
        Artist currentArtist = artists.get(position);
        holder.setArtistName(currentArtist.getName());
    }

    @Override
    public int getItemCount() {
        return artists.size();
    }

    public void addAll(@NonNull ArrayList<Artist> artists){
        if(artists == null){
            throw new NullPointerException("No puede se null");
        }
        this.artists.addAll(artists);
        //notifyItemRangeInserted(getItemCount()- 1, artists.size());
        notifyDataSetChanged();
    }

}
