package com.example.amoralesu.myapplication.domain;

/**
 * Created by amoralesu on 3/4/2016.
 */
public class Artist {
    String name;
    public Artist(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
