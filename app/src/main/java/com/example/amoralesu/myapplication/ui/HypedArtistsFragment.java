package com.example.amoralesu.myapplication.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.amoralesu.myapplication.ItemOffSetDecoration;
import com.example.amoralesu.myapplication.R;
import com.example.amoralesu.myapplication.domain.Artist;
import com.example.amoralesu.myapplication.io.model.HypedArtistsResponse;
import com.example.amoralesu.myapplication.io.model.LastFmApiAdapter;
import com.example.amoralesu.myapplication.ui.adapter.HypedArtistsAdapter;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by amoralesu on 3/3/2016.
 */
public class HypedArtistsFragment extends Fragment implements Callback<HypedArtistsResponse> {

    public static String LOG_TAG = HypedArtistsFragment.class.getSimpleName();
    public static final int NUM_COLUMS = 2;
    private RecyclerView mHypedArtistsFragment;

    private HypedArtistsAdapter adapter;


    @Override
    public  void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        adapter =  new HypedArtistsAdapter(getActivity());
    }

    //    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        if(activity instanceof MainActivity){
//            Log.i(LOG_TAG,"Yes");
//        }
//    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_hyped_artists, container, false);
        mHypedArtistsFragment = (RecyclerView) root.findViewById(R.id.rv);

        setupArtists();
        //setData();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        LastFmApiAdapter.getApiService().getHypedArtist(this);
    }

    //LayoutManager: define como se comportar en distribucuib
    //lista o grilla
    private void setupArtists() {
        mHypedArtistsFragment.setLayoutManager(new GridLayoutManager(getActivity(), NUM_COLUMS));
        mHypedArtistsFragment.setAdapter(adapter);
        mHypedArtistsFragment.addItemDecoration(new ItemOffSetDecoration(getActivity(), R.integer.offset));

    }

    private void setData(){
        ArrayList<Artist> arrayList =  new ArrayList<>();

        for (int i = 0; i < 11 ; i++) {
            arrayList.add(new Artist("name " + i ));
        }
        adapter.addAll(arrayList);
    }

    @Override
    public void success(HypedArtistsResponse hypedArtistsResponse, Response response) {
        adapter.addAll(hypedArtistsResponse.getArtist());
    }

    @Override
    public void failure(RetrofitError error) {
        error.printStackTrace();
    }

    //actividad = contexto

    /*
    * Adaptador
    * modelo datos + vista
    *
    * */

}
