package com.example.amoralesu.myapplication.io;

import com.example.amoralesu.myapplication.ui.HypedArtistsFragment;

import retrofit.http.GET;


/**
 * Created by amoralesu on 3/5/2016.
 */
public interface LastFmApiService {

    @GET(ApiConstans.URL_HYPED_ARTISTS)
    void getHypedArtist(HypedArtistsFragment serverResponse);
}
