package com.example.amoralesu.myapplication;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.IntegerRes;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by amoralesu on 3/5/2016.
 */
public class ItemOffSetDecoration extends RecyclerView.ItemDecoration {
    //espacio px
    private int itemOffSet;

    public ItemOffSetDecoration(Context context ,@IntegerRes int intResId) {
        int itemOffSetDp =context.getResources().getInteger(intResId);
        itemOffSet = convertPx(itemOffSetDp , context.getResources().getDisplayMetrics());

    }

    //DPs To Pxs
    public int convertPx(int offsetDp , DisplayMetrics metrics){
        return  offsetDp * (metrics.densityDpi / 160);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(itemOffSet,itemOffSet,itemOffSet,itemOffSet);
    }
}
